/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.jlogserver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 * FileUtils
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class FileUtils {

    private static final Logger logger = Logger.getLogger(FileUtils.class);

    /**
     * Recursively deletes a directory and all its children.
     *
     * @param f
     */
    public static void delete(File f) {
        logger.error("FileUtils::delete - " + f.getAbsolutePath());
        if (f.isDirectory()) {
            File[] children = f.listFiles();
            for (File child : children) {
                if (child.isDirectory()) {
                    delete(child);
                } else if (child.isFile()) {
                    logger.error("FileUtils::delete - Deleting " + child.getAbsolutePath());
                    child.delete();
                }
            }
        }
        if (logger.isInfoEnabled()) {
            logger.info("FileUtils::delete - Deleting " + f.getAbsolutePath());
            f.delete();
        }
    }

    public static void createFolder(String pathToDir) throws IOException {
        File dir = new File(pathToDir);
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                throw new IOException("IOException: Could not create directory " + pathToDir);
            }
        }
    }

    public static String getFileNameWOEnding(String fileName) {
        if (!fileName.contains(".")) {
            return fileName;
        }
        return fileName.substring(0, fileName.lastIndexOf("."));
    }

    public static String getFileEnding(String fileName) {
        if (!fileName.contains(".")) {
            return "";
        }
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

    public static String getWOEndingSlash(String fileName) {
        return fileName.endsWith(File.separator) ? fileName.substring(0, fileName.length() - 1) : fileName;
    }

    /**
     * log file path structure:
     * pathToLogfiles/knownOrUnknown/key/appName/appVersion/appBuild/date/filename_hh:mm:ss.fileEnding
     */
    public static String createNewLogPathAndFile(
            String pathToLogfiles,
            String key,
            boolean keyKnown,
            Date date,
            String appName,
            String appVersion,
            String appBuild,
            String fileName,
            String fileContent) throws IOException {
        // create main directory
        String pathToLogFileFolder = getWOEndingSlash(pathToLogfiles);
        try {
            createFolder(pathToLogFileFolder);
        } catch (IOException ex) {
            logger.error("FileUtils::createNewLogPathAndFile - IOException: Could not create log main directory " + pathToLogFileFolder);
            throw ex;
        }
        // create known/unknown directory
        String pathToknownUnknownFolder = getWOEndingSlash(pathToLogFileFolder + File.separator + (keyKnown ? "known" : "unknown"));
        try {
            createFolder(pathToknownUnknownFolder);
        } catch (IOException ex) {
            logger.error("FileUtils::createNewLogPathAndFile - IOException: Could not create known/unknown directory " + pathToknownUnknownFolder);
            throw ex;
        }
        // create key directory
        String pathToKeyDir = getWOEndingSlash(pathToknownUnknownFolder + File.separator + key);
        try {
            createFolder(pathToKeyDir);
        } catch (IOException ex) {
            logger.error("FileUtils::createNewLogPathAndFile - IOException: Could not create key directory " + pathToKeyDir);
            throw ex;
        }
        // create appName directory
        String pathToAppNameDir = getWOEndingSlash(pathToKeyDir + File.separator + appName);
        try {
            createFolder(pathToAppNameDir);
        } catch (IOException ex) {
            logger.error("FileUtils::createNewLogPathAndFile - IOException: Could not create appName directory " + pathToAppNameDir);
            throw ex;
        }
        // create appVersion directory
        String pathToAppVersionDir = getWOEndingSlash(pathToAppNameDir + File.separator + appVersion);
        try {
            createFolder(pathToAppVersionDir);
        } catch (IOException ex) {
            logger.error("FileUtils::createNewLogPathAndFile - IOException: Could not create appVersion directory " + pathToAppVersionDir);
            throw ex;
        }
        // create appBuild directory
        String pathToAppBuildDir = getWOEndingSlash(pathToAppVersionDir + File.separator + appBuild);
        try {
            createFolder(pathToAppBuildDir);
        } catch (IOException ex) {
            logger.error("FileUtils::createNewLogPathAndFile - IOException: Could not create appBuild directory " + pathToAppBuildDir);
            throw ex;
        }
        // create date directory
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = (date == null) ? "unknown" : df1.format(date);
        String pathToDateDir = getWOEndingSlash(pathToAppBuildDir + File.separator + date1);
        try {
            createFolder(pathToDateDir);
        } catch (IOException ex) {
            logger.error("FileUtils::createNewLogPathAndFile - IOException: Could not create date directory " + pathToDateDir);
            throw ex;
        }
        // create log file  
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm:ss");
        String date2 = (date == null) ? "unknown" : df2.format(date);
        date2 = date2.replaceAll(":", "-");
        String fileEnding = getFileEnding(fileName);
        fileEnding = fileEnding.isEmpty() ? fileEnding : ("." + fileEnding);
        String pathToLogFile = pathToDateDir + File.separator + getFileNameWOEnding(fileName) + "_" + date2 + fileEnding;
        try {
            writeToFile(pathToLogFile, fileContent);
            return pathToLogFile;
        } catch (IOException ex) {
            logger.error("FileUtils::createNewLogPathAndFile - IOException: Could not create log file " + pathToLogFile);
            throw ex;
        }
    }

    public static void writeToFile(String pathToFile, String text) throws IOException {
        OutputStreamWriter out = null;
        File f = new File(pathToFile);
        try {
            if (!f.exists() && !f.createNewFile()) {
                logger.error("FileUtils::writeToFile - Could not create file " + pathToFile);
                throw new IOException("Could not create file " + pathToFile);
            }
            if (f.canWrite()) {
                out = new OutputStreamWriter(new FileOutputStream(f), Charset.forName("UTF-8"));
                out.write(text);
                out.flush();
            } else {
                logger.error("FileUtils::writeToFile - Can't write to file " + pathToFile);
                throw new IOException("Can't write to file " + pathToFile);
            }
        } catch (IOException ex) {
            logger.error("FileUtils::writeToFile - IOException #1: " + ex.getMessage());
            throw ex;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    logger.error("FileUtils::writeToFile - IOException #2: " + ex.getMessage());
                    throw ex;
                }
            }
        }
    }
}
