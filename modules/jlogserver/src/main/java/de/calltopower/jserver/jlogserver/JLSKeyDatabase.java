/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.jlogserver;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * JLSKeyDatabase
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class JLSKeyDatabase {

    private static final Logger logger = Logger.getLogger(JLSKeyDatabase.class);
    private List<String> keys = null;

    public JLSKeyDatabase() {
        keys = new LinkedList<>();
    }

    public List<String> getKeys() {
        return keys;
    }

    public void loadDatabase(String databasePath) throws IOException {
        if (keys != null) {
            keys.clear();
        }
        keys = new LinkedList<>();
        InputStream is = null;
        try {
            File f = new File(databasePath);
            if (!f.exists()) {
                if (!f.createNewFile()) {
                    logger.error("JLSKeyDatabase::loadDatabase - Cannot create key database file: " + databasePath);
                    throw new IOException("JLSKeyDatabase::loadDatabase - Cannot create key database file: " + databasePath);
                } else {
                    if (logger.isInfoEnabled()) {
                        logger.info("JLSKeyDatabase::loadDatabase - Created empty key database file at: " + databasePath);
                    }
                }
            }
            if (f.isFile() && f.canRead()) {
                if (logger.isInfoEnabled()) {
                    logger.info("JLSKeyDatabase::loadDatabase - Found key database file at: " + databasePath);
                }
                BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
                String line;
                while ((line = in.readLine()) != null) {
                    if (!line.isEmpty()) {
                        keys.add(line);
                    }
                }
            } else {
                logger.error("JLSKeyDatabase::loadDatabase - Cannot read file or is not a file: " + databasePath);
                throw new IOException("JLSKeyDatabase::loadDatabase - Cannot read file or is not a file: " + databasePath);
            }
        } catch (IOException ex) {
            logger.error("JLSKeyDatabase::loadDatabase - Failed to load key database: " + ex.getMessage());
            throw ex;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    logger.error("JLSKeyDatabase::loadDatabase - Failed to close stream: " + ex.getMessage());
                    throw ex;
                }
            }
        }
    }
}
