/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.lock;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import org.apache.log4j.Logger;

/**
 * Lock
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class Lock {

    private static final Logger logger = Logger.getLogger(Lock.class);
    private final String appName;
    private File file;
    private FileChannel channel;
    private FileLock lock;

    public Lock(String appName) {
        this.appName = appName;
    }

    public boolean otherInstanceIsRunning() {
        if (logger.isInfoEnabled()) {
            logger.info("Lock::otherInstanceIsRunning - Checking for other instances named '" + this.appName + "'");
        }
        try {
            file = new File(System.getProperty(Constants.JLS_LOCK_FILE_LOCATION), appName + Constants.JLS_LOCK_FILE_SUFFIX);
            if (logger.isInfoEnabled()) {
                logger.info("Lock::otherInstanceIsRunning - Checking for file '" + file.getAbsolutePath() + "'");
            }
            channel = new RandomAccessFile(file, "rw").getChannel();

            try {
                lock = channel.tryLock();
            } catch (OverlappingFileLockException e) {
                // already locked
                logger.error("Lock::otherInstanceIsRunning - An instance is already running: " + e.getMessage());
                closeLock();
                return true;
            }

            if (lock == null) {
                closeLock();
                return true;
            }

            Runtime.getRuntime().addShutdownHook(new Thread() {
                // destroy the lock when the JVM is closing
                @Override
                public void run() {
                    if (logger.isInfoEnabled()) {
                        logger.info("Lock::otherInstanceIsRunning - JVM is closing - closing the lock");
                    }
                    closeLock();
                    deleteFile();
                }
            });
            return false;
        } catch (Exception e) {
            logger.error("Lock::otherInstanceIsRunning - Can't lock that file: " + e.getMessage());
            closeLock();
            return true;
        }
    }

    private void closeLock() {
        try {
            lock.release();
        } catch (Exception e) {
            logger.error("Lock::closeLock - while releasing the lock: " + e.getMessage());
        }
        try {
            channel.close();
        } catch (Exception e) {
            logger.error("Lock::closeLock - while closing the channel: " + e.getMessage());
        }
    }

    private void deleteFile() {
        try {
            file.delete();
        } catch (Exception e) {
            logger.error("Lock::deleteFile - while deleting the file: " + e.getMessage());
        }
    }
}
