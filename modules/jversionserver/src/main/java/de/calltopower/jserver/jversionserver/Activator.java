/*
 * Copyright (c) 2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.jserver.jversionserver;

import de.calltopower.jserver.jversionserver.conf.Configuration;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.Map.Entry;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

/**
 * Activator
 *
 * @date 21.02.2015
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class Activator implements BundleActivator {

    private static final Logger logger = Logger.getLogger(Activator.class);
    private Configuration applicationConfig;
    private BundleContext bc;
    private List<String> httpServiceList;
    private HttpService service = null;
    private String registerURL;

    @Override
    public void start(BundleContext _bc) throws Exception {
        this.bc = _bc;

        if (logger.isInfoEnabled()) {
            logger.info("jVersionServer Version " + Constants.JVS_VERSION + " Build " + Constants.JVS_BUILD + " by Denis Meyer");
        }

        // load configuration and register as service
        applicationConfig = new Configuration(loadConfiguration());
        Hashtable props = new Hashtable();
        props.put("description", "full application configuration");
        bc.registerService(applicationConfig.getClass().getName(), applicationConfig, props);
        registerURL = Constants.str_versionPath + applicationConfig.get(Constants.PROPKEY_JVS_API_VERSION);

        // register http services
        httpServiceList = new LinkedList<>();
        if (!registerServlets()) {
            if (bc != null) {
                if (logger.isInfoEnabled()) {
                    logger.info("Activator::start - Stopping module...");
                }
                this.stop(bc);
            } else {
                logger.error("Activator::start - Could not get bundle context...");
            }
        }
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if (service != null) {
            for (String sr : httpServiceList) {
                service.unregister(sr);
            }
        }
    }

    private boolean registerServlets() {
        if (logger.isInfoEnabled()) {
            logger.info("Activator::registerServlets");
        }
        ServiceReference sRef = bc.getServiceReference(HttpService.class.getName());
        if (sRef != null) {
            if (logger.isInfoEnabled()) {
                logger.info("Activator::registerServlets - Got service reference for " + HttpService.class.getName());
            }
            try {
                service = (HttpService) bc.getService(sRef);
                httpServiceList.add(registerURL);
                if (logger.isInfoEnabled()) {
                    logger.info("Activator::registerServlets - Registered servlet at " + registerURL);
                }
                service.registerServlet(registerURL, new JVSServlet(applicationConfig), null, null);
                return true;
            } catch (ServletException ex) {
                logger.error("Activator::registerServlets - ServletException: " + ex.getMessage());
                logger.error(ex.getMessage());
                return false;
            } catch (NamespaceException ex) {
                logger.error("Activator::registerServlets - NamespaceException: " + ex.getMessage());
                logger.error(ex.getMessage());
                return false;
            }
        } else {
            logger.error("Activator::registerServlets - Could not get service reference for " + HttpService.class.getName());
            return false;
        }
    }

    private Properties loadConfiguration() {
        InputStream is;
        Properties defaults = new Properties();

        // load defaults from bundle
        try {
            is = getClass().getResourceAsStream(Constants.DEFAULT_CONFIG_PATH);
            defaults.load(is);
            is.close();
            // printProperties(defaults);
        } catch (Exception e) {
            logger.error("Activator::loadConfiguration - Failed to load default configuration: " + e.getMessage());
        }

        boolean configUserLoaded = false;
        // load user config
        Properties configUser = new Properties(defaults);
        try {
            is = new FileInputStream(new File(Constants.CONFIG_PATH));
            configUser.load(is);
            is.close();
            configUserLoaded = true;
        } catch (IOException e) {
            logger.warn("Activator::loadConfiguration - user config file not found at '" + Constants.CONFIG_PATH + "', application defaults will be used!");
        }

        // print and return config
        if (configUserLoaded) {
            printProperties(configUser, "Changed config property");
            return configUser;
        }
        printProperties(defaults, "Default config property");
        return defaults;

    }

    private void printProperties(Properties props, String prefix) {
        if (logger.isInfoEnabled()) {
            for (Entry entry : props.entrySet()) {
                String key = (String) entry.getKey();
                String value = (String) entry.getValue();
                String pref = ((prefix == null) || prefix.isEmpty()) ? "" : prefix + " - ";
                logger.info(pref + key + " = " + value);
            }
        }
    }
}
